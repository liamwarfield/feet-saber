AFRAME.registerComponent('trying-my-best', {
  schema: {
      default: '',
      parse: AFRAME.utils.styleParser.parse
  },

  init: function () {
    const el = this.el;
    sphere = document.getElementById("oddball")

    el.addEventListener("buttondown", evt => {
      console.log("Move");
      sphere.object3D.position.x += 5;
    });
    el.addEventListener("buttonup", evt => {
      console.log("UnMove");
      sphere.object3D.position.x -= 5;
    });
  }
});
