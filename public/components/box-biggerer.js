AFRAME.registerComponent('box-biggerer', {
    init: function () {
      const el = this.el;
      this.isGrowwing = false;
      this.scale = 1.0;
      this.box = document.getElementById("box")
      console.log("Making boxxes bigger")
  
      el.addEventListener("buttondown", evt => {
        console.log("Bigger");
        this.isGrowwing = true;
        console.log(el)
      });
      el.addEventListener("buttonup", evt => {
        console.log("smaller")
        this.isGrowwing = false;
        console.log(el)
      });
    },
  
    tick: function () {
      const box = this.box;
      let scale = this.scale;
      if (this.isGrowwing) {
        box.object3D.scale.x += .01;
        box.object3D.scale.y += .01;
        box.object3D.scale.z += .01;
        this.scale += .01;
      } else if(scale > 1) {
        box.object3D.scale.x -= .01;
        box.object3D.scale.y -= .01;
        box.object3D.scale.z -= .01;
        this.scale -= .01;
      }
    }
});